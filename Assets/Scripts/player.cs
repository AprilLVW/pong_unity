﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidadY;

    public float maxY;

    private float posy;

    private float direction;


    // Update is called once per frame
    void Update()
    {
        
        direction = Input.GetAxis("Vertical");

        posy = transform.position.y + direction*velocidadY*Time.deltaTime;


            if (posy>maxY){
                posy = maxY;
            }

            else if (posy<-maxY){
                posy = -maxY;
            }

        transform.position = new Vector3(transform.position.x, posy, transform.position.z);
    }
}
