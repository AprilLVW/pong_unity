﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball3 : MonoBehaviour

{
  public Vector2 velocity;

  private Rigidbody2D rigid;

  void Start() {
      rigid = GetComponent<Rigidbody2D>();

      rigid.AddForce(velocity, ForceMode2D.Impulse);
  }
}
